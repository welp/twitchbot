use dotenv::dotenv;
use redis::Commands;
use slack_api as slack;
use std::env;

#[derive (Debug)]
struct Stream {
    game: String,
    live: bool,
    title: String,
    user_id: String,
    user_name: String,
}

fn make_twitch_request(
    url_string: &str,
    key: &str,
    sec: &str,
) -> serde_json::Value {
    let auth_header = format!("Bearer {}", &sec);
    return match ureq::get(&url_string)
        .set("Authorization", &auth_header)
        .set("Client-ID", &key)
        .call()
        .into_json() {
            Ok(blob) => blob,
            Err(err) => panic!("Failed to fetch {}: {:?}",
                               &url_string,
                               err),
        };
}

fn get_user_id(user: &str, key: &str, sec: &str) -> String {
    let url_string = format!("https://api.twitch.tv/helix/users?login={}", &user);
    let blob = make_twitch_request(&url_string, &key, &sec);

    return String::from(blob["data"][0]["id"].as_str().unwrap())
}

fn get_stream_info(id: &str, key: &str, sec: &str) -> Stream {
    let url_string = format!("https://api.twitch.tv/helix/streams?user_id={}", &id);
    let blob = make_twitch_request(&url_string, &key, &sec);

    if blob["data"][0].is_null() {
        return Stream {
            game: String::from(""),
            live: false,
            title: String::from(""),
            user_id: String::from(id),
            user_name: String::from(""),
        }
    }

    let stream_title = String::from(blob["data"][0]["title"].as_str().unwrap());
    let username = String::from(blob["data"][0]["user_name"].as_str().unwrap());

    let game_id = String::from(blob["data"][0]["game_id"].as_str().unwrap());
    let game_url = format!("https://api.twitch.tv/helix/games?id={}", &game_id);
    let game_blob = make_twitch_request(&game_url, &key, &sec);
    let game = if game_blob["data"][0]["name"].is_null() {
        "who knows what"
    } else {
        game_blob["data"][0]["name"].as_str().unwrap()
    };

    Stream {
        game: String::from(game),
        live: true,
        title: stream_title,
        user_id: String::from(id),
        user_name: username,
    }
}

fn make_redis_connection() -> std::result::Result<redis::Connection, redis::RedisError> {
    let redis_host = env::var("REDIS_HOST").expect("REDIS_HOST must have a value");
    let redis_pass = match env::var("REDIS_PASSWORD") {
        Ok(val) => val,
        Err(_) => String::from(""),
    };
    let connection_string = format!("redis://:{}@{}:6379/",
                                    redis_pass,
                                    redis_host);

    let client = redis::Client::open(connection_string)?;
    let connection = client.get_connection()?;
    Ok(connection)
}

fn stream_last_status_was_live(stream: &Stream, connection: &mut redis::Connection) -> bool {
    let stream_value: Option<String> = connection.get(&stream.user_id).unwrap();
    if stream_value == Some(String::from("1")) {
        return true
    } else {
        return false
    };
}

async fn slack_joined_channels(token: &str) -> Vec<slack::Channel> {
    let slack_client = slack::default_client().unwrap();
    let resp = slack::channels::list(
        &slack_client,
        &token,
        &slack::channels::ListRequest {
            exclude_archived: Some(true),
            exclude_members: Some(false),
        }
    ).await;

    let channels = match resp {
        Ok(val) => match val.channels {
            Some(chs) => chs,
            None => panic!("No channels found"),
        },
        Err(err) => panic!("Failed to fetch channels: {:?}", err),
    };

    return channels
        .iter()
        .filter(|ch| ch.is_member == Some(true))
        .map(|ch| ch.clone())
        .collect()
}

async fn slack_send_alert(
    token: &str,
    channel: &slack::Channel,
    stream: &Stream,
) -> slack::chat::PostMessageResponse {
    let slack_client = slack::default_client().unwrap();

    let twitch_url = format!("https://twitch.tv/{}", stream.user_name.to_lowercase());
    let message_text = format!(
        "oh shit {} is playing {} {}",
        stream.user_name.to_lowercase(),
        stream.game.to_lowercase(),
        twitch_url,
    );

    return slack::chat::post_message(
        &slack_client,
        &token,
        &slack::chat::PostMessageRequest {
            channel: &channel.id.as_ref().unwrap(),
            text: &message_text,
            ..Default::default()
        }
    ).await.unwrap();
}

async fn alert_if_live(
    slack_token: &str,
    twitch_client: &str,
    twitch_token: &str,
    user: &str
) -> () {
    let id = get_user_id(user, &twitch_client, &twitch_token);
    let stream = get_stream_info(&id, &twitch_client, &twitch_token);

    let mut connection = make_redis_connection().unwrap();
    let was_live = stream_last_status_was_live(&stream, &mut connection);

    let slack_channels = slack_joined_channels(&slack_token).await;

    if was_live {
        if stream.live {
            println!("{} is still chuggin along!", &stream.user_name)
        } else {
            println!("{} has [monty python voice] gone off", &stream.user_id);
            let _: () = connection.set(&stream.user_id, 0).unwrap();
        }
    } else {
        if stream.live {
            println!("OH SHIT ITS GO TIME {} IS LIVE", &stream.user_name);
            slack_send_alert(&slack_token, &slack_channels[0], &stream).await;
            let _: () = connection.set(&stream.user_id, 1).unwrap();
        } else {
            println!("{} still offline, so boring", &stream.user_id)
        }
    }
}

#[tokio::main]
async fn main() {
    dotenv().ok();

    let twitch_client = env::var("TWITCH_CLIENT").expect("TWITCH_CLIENT must be set");
    let twitch_token = env::var("TWITCH_TOKEN").expect("TWITCH_TOKEN must be set");
    let slack_token = env::var("SLACK_TOKEN").expect("SLACK_TOKEN must be set");

    let twitch_users: Vec<String> = match env::var("TWITCH_USERS") {
        Ok(users) => users.split(",").map(|u| String::from(u)).collect(),
        Err(_) => panic!("TWITCH_USERS must be set"),
    };

    for user in twitch_users {
        alert_if_live(&slack_token, &twitch_client, &twitch_token, &user).await;
    }
}
