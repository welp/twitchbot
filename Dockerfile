ARG DEBIAN_VERSION=10.5
ARG RUST_VERSION=1.44

FROM rust:${RUST_VERSION}-slim as builder
RUN apt-get -yq update && \
  apt-get -yq upgrade && \
  DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
    build-essential \
    libssl-dev \
    pkg-config

WORKDIR /twusp

# https://dev.to/deciduously/use-multi-stage-docker-builds-for-statically-linked-rust-binaries-3jgd
RUN rustup target add x86_64-unknown-linux-gnu
RUN USER=root cargo init
COPY Cargo.toml Cargo.lock ./
# run this separately to cache dependencies
RUN cargo install \
  --path . \
  --root /build \
  --target x86_64-unknown-linux-gnu

# copy over the actual project files
COPY . .

RUN rm -rf target/release
RUN rm -rf target/debug

RUN rm -rf target/x86_64-unknown-linux-gnu/release/deps/libtwusp*
RUN rm -rf target/x86_64-unknown-linux-gnu/release/deps/twusp*
RUN rm -rf target/x86_64-unknown-linux-gnu/release/incremental
RUN rm -rf target/x86_64-unknown-linux-gnu/release/twusp*

RUN cargo install \
  --path . \
  --root /build \
  --target x86_64-unknown-linux-gnu \
  --force

FROM debian:${DEBIAN_VERSION}-slim
RUN apt-get -yq update && \
  apt-get upgrade -yq && \
  DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
    libssl-dev \
    ca-certificates \
  && apt-get -y clean && \
  rm -rf /var/lib/apt/lists/*

COPY --from=builder /build/bin/twusp /opt/twusp/twusp

WORKDIR /opt/twusp
CMD ["/opt/twusp/twusp"]
